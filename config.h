/* See LICENSE file for copyright and license details. */

/* Constants */
#define DEFAULT_TERMINAL "st"
#define TERMCLASS "St"
#define BROWSER "brave"

/* appearance */
static const Gap default_gap        = {.isgap = 1, .realgap = 10, .gappx = 10};
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static unsigned int borderpx  = 1;        /* border pixel of windows */
static unsigned int snap      = 32;       /* snap pixel */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static char font[]            = "monospace:size=10";
static char font2[]            = "Noto Color Emoji:size=10";
static char dmenufont[]       = "monospace:size=10";
static const char *fonts[]          = { font, font2 };
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char urg_fg[]                = "#eeeeee";
static char urg_bg[]                = "#005577";
static char col_urgborder[]         = "#ff0000";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
	     [SchemeUrg]  = { urg_fg,      urg_bg,      col_urgborder  },
};
static char termcol0[] = "#000000"; /* black   */
static char termcol1[] = "#ff0000"; /* red     */
static char termcol2[] = "#33ff00"; /* green   */
static char termcol3[] = "#ff0099"; /* yellow  */
static char termcol4[] = "#0066ff"; /* blue    */
static char termcol5[] = "#cc00ff"; /* magenta */
static char termcol6[] = "#00ffff"; /* cyan    */
static char termcol7[] = "#d0d0d0"; /* white   */
static char termcol8[]  = "#808080"; /* black   */
static char termcol9[]  = "#ff0000"; /* red     */
static char termcol10[] = "#33ff00"; /* green   */
static char termcol11[] = "#ff0099"; /* yellow  */
static char termcol12[] = "#0066ff"; /* blue    */
static char termcol13[] = "#cc00ff"; /* magenta */
static char termcol14[] = "#00ffff"; /* cyan    */
static char termcol15[] = "#ffffff"; /* white   */
static char *termcolor[] = {
	termcol0,
	termcol1,
	termcol2,
	termcol3,
	termcol4,
	termcol5,
	termcol6,
	termcol7,
	termcol8,
	termcol9,
	termcol10,
	termcol11,
	termcol12,
	termcol13,
	termcol14,
	termcol15,
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 *  WM_WINDOW_ROLE(STRING) = role
	 */
	/* class                  role   instance  title           tags mask  isfloating  isfreesize  isterminal  noswallow  monitor */
	{ "Gimp",                 NULL,  NULL,     NULL,           0,         1,          1,           0,           0,        -1 },
	{ "Firefox",              NULL,  NULL,     NULL,           1 << 8,    0,          0,           0,          -1,        -1 },
	{ "St",                   NULL,  NULL,     NULL,           0,         0,          0,           1,           0,        -1 },
	{ "Tk",                   NULL,  NULL,     NULL,           0,         1,          1,           0,           0,        -1 },
	{ NULL,                   NULL,  NULL,     "Event Tester", 0,         0,          0,           0,           1,        -1 }, /* xev */
	{ "TelegramDesktop",      NULL,  NULL,     NULL,           0,         1,          0,           0,           0,        -1 },
	{ "Lutris",               NULL,  NULL,     NULL,           8,         1,          1,           0,           0,        -1 },
	{ "riotclientux.exe",     NULL,  NULL,     NULL,           8,         1,          1,           0,           0,        -1 },
	{ "leagueclientux.exe",   NULL,  NULL,     NULL,           8,         1,          1,           0,           0,        -1 },
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static int attachbelow = 1;    /* 1 means attach after the currently active window */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { DEFAULT_TERMINAL, NULL };

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "font",               STRING,  &font },
		{ "font2",              STRING,  &font2 },
		{ "dmenufont",          STRING,  &dmenufont },
		{ "color0",             STRING,  &normbgcolor },
		{ "color8",             STRING,  &normbordercolor },
		{ "color15",            STRING,  &normfgcolor },
		{ "color9",             STRING,  &selbgcolor },
		{ "color9",             STRING,  &selbordercolor },
		{ "color15",            STRING,  &selfgcolor },
		{ "color1",             STRING,  &urg_bg },
		{ "color1",             STRING,  &col_urgborder },
		{ "color15",            STRING,  &urg_fg },
		{ "borderpx",          	INTEGER, &borderpx },
		{ "snap",           		INTEGER, &snap },
		{ "showbar",          	INTEGER, &showbar },
		{ "topbar",           	INTEGER, &topbar },
		{ "nmaster",          	INTEGER, &nmaster },
		{ "resizehints",       	INTEGER, &resizehints },
		{ "mfact",      	    	FLOAT,   &mfact },
		{ "color0",             STRING,   &termcol0 },
		{ "color1",             STRING,   &termcol1 },
		{ "color2",             STRING,   &termcol2 },
		{ "color3",             STRING,   &termcol3 },
		{ "color4",             STRING,   &termcol4 },
		{ "color5",             STRING,   &termcol5 },
		{ "color6",             STRING,   &termcol6 },
		{ "color7",             STRING,   &termcol7 },
		{ "color8",             STRING,   &termcol8 },
		{ "color9",             STRING,   &termcol9 },
		{ "color10",            STRING,   &termcol10 },
		{ "color11",            STRING,   &termcol11 },
		{ "color12",            STRING,   &termcol12 },
		{ "color13",            STRING,   &termcol13 },
		{ "color14",            STRING,   &termcol14 },
		{ "color15",            STRING,   &termcol15 },
};

#include <X11/XF86keysym.h>

static const Key keys[] = {
	/* modifier                     key        function        argument */
	// apps
	{ MODKEY,                       XK_q,          killclient,             {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,          quit,                   {1} },
	{ MODKEY|ShiftMask,             XK_q,          quit,                   {0} },

	{ MODKEY,                       XK_d,          spawn,                  {.v = dmenucmd } },
	{ MODKEY,                       XK_Return,     spawn,                  {.v = termcmd } },
	{ MODKEY,                       XK_b,          togglebar,              {0} },
	{ MODKEY,                       XK_w,          spawn,                  {.v = (const char*[]){ BROWSER, NULL } } },
	{ MODKEY|ShiftMask,             XK_w,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "sudo", "nmtui", NULL } } },
	{ MODKEY,                       XK_r,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "lfub", NULL } } },
	{ MODKEY|ShiftMask,             XK_r,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "htop", NULL } } },
	{ MODKEY|ShiftMask,             XK_n,          spawn,                  SHCMD(DEFAULT_TERMINAL " -e newsboat ; kill -40 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_e,          spawn,                  SHCMD(DEFAULT_TERMINAL " -e neomutt ; kill -46 $(pidof dwmblocks); rmdir ~/.abook") },

	// larbs.xyz features
	{ MODKEY,                       XK_F1,         spawn,                  SHCMD("groff -mom /usr/local/share/dwm/larbs.mom -Tpdf | zathura -") },
	{ MODKEY,                       XK_F2,         spawn,                  {.v = (const char*[]){ "tutorialvids", NULL } } },
	{ MODKEY,                       XK_F3,         spawn,                  {.v = (const char*[]){ "displayselect", NULL } } },
	{ MODKEY,                       XK_F4,         spawn,                  SHCMD(DEFAULT_TERMINAL " -e pulsemixer; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_F5,         spawn,                  SHCMD("xrdb -merge ~/.config/x11/xresources") },
	{ MODKEY,                       XK_F6,         spawn,                  {.v = (const char*[]){ "torwrap", NULL } } },
	{ MODKEY,                       XK_F7,         spawn,                  {.v = (const char*[]){ "td-toggle", NULL } } },
	{ MODKEY,                       XK_F8,         spawn,                  {.v = (const char*[]){ "mw", "-Y", NULL } } },
	{ MODKEY,                       XK_F9,         spawn,                  {.v = (const char*[]){ "dmenumount", NULL } } },
	{ MODKEY,                       XK_F10,        spawn,                  {.v = (const char*[]){ "dmenuumount", NULL } } },
	{ MODKEY,                       XK_F11,        spawn,                  SHCMD("mpv --untimed --no-cache --no-osc --no-input-default-bindings --profile=low-latency --input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | tail -n 1)") },
	{ MODKEY,                       XK_F12,        spawn,                  SHCMD("remaps") },
	{ MODKEY,                       XK_Insert,     spawn,                  SHCMD("xdotool type $(grep -v '^#' ~/.local/share/personal/bookmarks | dmenu -i -l 50 | cut -d' ' -f1)") },
	{ MODKEY,                       XK_BackSpace,  spawn,                  {.v = (const char*[]){ "sysact", NULL } } },
	{ MODKEY,                       XK_grave,      spawn,                  {.v = (const char*[]){ "dmenuunicode", NULL } } },

	// screenshot & record
	{ 0,                            XK_Print,      spawn,                  SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ ShiftMask,                    XK_Print,      spawn,                  {.v = (const char*[]){ "maimpick", NULL } } },
	{ MODKEY,                       XK_Print,      spawn,                  {.v = (const char*[]){ "dmenurecord", NULL } } },
	{ MODKEY|ShiftMask,             XK_Print,      spawn,                  {.v = (const char*[]){ "dmenurecord", "kill", NULL } } },
	{ MODKEY,                       XK_Delete,     spawn,                  {.v = (const char*[]){ "dmenurecord", "kill", NULL } } },
	{ MODKEY,                       XK_Scroll_Lock, spawn,                  SHCMD("killall screenkey || screenkey &") },

	// sound and media
	{ MODKEY,                       XK_m,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "ncmpcpp", NULL } } },
	{ MODKEY|ShiftMask,             XK_m,          spawn,                  SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_comma,      spawn,                  {.v = (const char*[]){ "mpc", "prev", NULL } } },
	{ MODKEY,                       XK_period,     spawn,                  {.v = (const char*[]){ "mpc", "next", NULL } } },
	{ MODKEY,                     XK_bracketleft,  spawn,                  {.v = (const char*[]){ "mpc", "seek", "-10", NULL } } },
	{ MODKEY|ShiftMask,           XK_bracketleft,  spawn,                  {.v = (const char*[]){ "mpc", "seek", "-60", NULL } } },
	{ MODKEY,                     XK_bracketright, spawn,                  {.v = (const char*[]){ "mpc", "seek", "+10", NULL } } },
	{ MODKEY|ShiftMask,           XK_bracketright, spawn,                  {.v = (const char*[]){ "mpc", "seek", "+60", NULL } } },
	{ MODKEY,                       XK_p,          spawn,                  {.v = (const char*[]){ "mpc", "toggle", NULL } } },
	{ MODKEY|ShiftMask,             XK_p,          spawn,                  SHCMD("mpc pause; pauseallmpv") },
	{ MODKEY,                       XK_minus,      spawn,                  SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_minus,      spawn,                  SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_equal,      spawn,                  SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_equal,      spawn,                  SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)") },

	// stacks
	{ MODKEY,                       XK_v,          focusstack,             {.i =  0 } },
	{ MODKEY,                       XK_j,          focusstack,             {.i = +1 } },
	{ MODKEY,                       XK_k,          focusstack,             {.i = -1 } },
	{ MODKEY,                       XK_h,          setmfact,               {.f = -0.05} },
	{ MODKEY,                       XK_l,          setmfact,               {.f = +0.05} },

	{ MODKEY|ShiftMask,             XK_h,          setcfact,               {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_l,          setcfact,               {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_BackSpace,  setcfact,               {0} },
	// { MODKEY|ShiftMask,             XK_j,          movestack,              {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_k,          movestack,              {.i = -1 } },
	{ MODKEY,                       XK_space,      zoom,                   {0} },

	// gaps
	{ MODKEY,                       XK_x,          setgaps,                {.i = -5 } },
	{ MODKEY,                       XK_z,          setgaps,                {.i = +5 } },
	{ MODKEY,                       XK_a,          setgaps,                {.i = GAP_TOGGLE } },
	{ MODKEY|ShiftMask,             XK_a,          setgaps,                {.i = GAP_RESET} },

	// layouts
	{ MODKEY,                       XK_t,          setlayout,              {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_t,          setlayout,              {.v = &layouts[1]} },
	{ MODKEY,                       XK_y,          setlayout,              {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_y,          setlayout,              {.v = &layouts[3]} },
	{ MODKEY,                       XK_u,          setlayout,              {.v = &layouts[4]} },
	{ MODKEY|ShiftMask,             XK_u,          setlayout,              {.v = &layouts[5]} },
	// { MODKEY,                       XK_i,          setlayout,              {.v = &layouts[6]} },
	// { MODKEY|ShiftMask,             XK_i,          setlayout,              {.v = &layouts[7]} },
	{ MODKEY,                       XK_o,          incnmaster,             {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_o,          incnmaster,             {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_f,          setlayout,              {.v = &layouts[6]} },
	/* { MODKEY|ShiftMask,             XK_f,          setlayout,              {0} }, */
	{ MODKEY|ShiftMask,             XK_space,      togglefloating,         {0} },
	{ MODKEY,                       XK_f,          togglefullscr,          {0} },
	// { MODKEY,                       XK_Tab,        toggleAttachBelow,      {0} },

	// monitors
	{ MODKEY,                       XK_Left,       focusmon,               {.i = -1 } },
	{ MODKEY,                       XK_Right,      focusmon,               {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Left,       tagmon,                 {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Right,      tagmon,                 {.i = +1 } },

	// tags
	{ MODKEY,                       XK_Tab,        view,                   {0} },
	// { MODKEY,                       XK_g,          shiftviewclients,       { .i = -1 } },
	// { MODKEY,                       XK_semicolon,  shiftviewclients,       { .i = +1 } },
	// { MODKEY,                       XK_0,          view,                   {.ui = ~SPTAGMASK } },
	// { MODKEY|ShiftMask,             XK_0,          tag,                    {.ui = ~SPTAGMASK } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

	// function keys
	{ 0, XF86XK_AudioMute,                         spawn,                  SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioRaiseVolume,                  spawn,                  SHCMD("pamixer --allow-boost -i 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioLowerVolume,                  spawn,                  SHCMD("pamixer --allow-boost -d 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioPrev,                         spawn,                  {.v = (const char*[]){ "mpc", "prev", NULL } } },
	{ 0, XF86XK_AudioNext,                         spawn,                  {.v = (const char*[]){ "mpc",  "next", NULL } } },
	{ 0, XF86XK_AudioPause,                        spawn,                  {.v = (const char*[]){ "mpc", "pause", NULL } } },
	{ 0, XF86XK_AudioPlay,                         spawn,                  {.v = (const char*[]){ "mpc", "toggle", NULL } } },
	{ 0, XF86XK_AudioStop,                         spawn,                  {.v = (const char*[]){ "mpc", "stop", NULL } } },
	{ 0, XF86XK_AudioRewind,                       spawn,                  {.v = (const char*[]){ "mpc", "seek", "-10", NULL } } },
	{ 0, XF86XK_AudioForward,                      spawn,                  {.v = (const char*[]){ "mpc", "seek", "+10", NULL } } },
	{ 0, XF86XK_AudioMedia,                        spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "ncmpcpp", NULL } } },
	{ 0, XF86XK_AudioMicMute,                      spawn,                  SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ 0, XF86XK_Calculator,                        spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "bc", "-l", NULL } } },
	{ 0, XF86XK_Sleep,                             spawn,                  {.v = (const char*[]){ "sudo", "-A", "zzz", NULL } } },
	{ 0, XF86XK_WWW,                               spawn,                  {.v = (const char*[]){ BROWSER, NULL } } },
	{ 0, XF86XK_DOS,                               spawn,                  {.v = termcmd } },
	{ 0, XF86XK_ScreenSaver,                       spawn,                  SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_TaskPane,                          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "htop", NULL } } },
	{ 0, XF86XK_Mail,                              spawn,                  SHCMD(DEFAULT_TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks") },
	{ 0, XF86XK_MyComputer,                        spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e",  "lfub",  "/", NULL } } },
	{ 0, XF86XK_Launch1,                           spawn,                  {.v = (const char*[]){ "xset", "dpms", "force", "off", NULL } } },
	{ 0, XF86XK_TouchpadToggle,                    spawn,                  SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOff,                       spawn,                  {.v = (const char*[]){ "synclient", "TouchpadOff=1", NULL } } },
	{ 0, XF86XK_TouchpadOn,                        spawn,                  {.v = (const char*[]){ "synclient", "TouchpadOff=0", NULL } } },
	{ 0, XF86XK_MonBrightnessUp,                   spawn,                  {.v = (const char*[]){ "xbacklight", "-inc", "15", NULL } } },
	{ 0, XF86XK_MonBrightnessDown,                 spawn,                  {.v = (const char*[]){ "xbacklight", "-dec", "15", NULL } } },
	/* { 0, XF86XK_Battery,		spawn,		SHCMD("") }, */
	/* { 0, XF86XK_PowerOff,		spawn,		{.v = (const char*[]){ "sysact", NULL } } }, */
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

