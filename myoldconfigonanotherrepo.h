/* See LICENSE file for copyright and license details. */

/* Constants */
#define DEFAULT_TERMINAL "st"
#define TERMCLASS "St"
#define BROWSER "brave"

/* appearance */
static const unsigned int borderpx       = 3;   /* border pixel of windows */
static const unsigned int snap           = 32;  /* snap pixel */
static const int swallowfloating         = 0;   /* 1 means swallow floating windows by default */
static const unsigned int gappih         = 20;  /* horiz inner gap between windows */
static const unsigned int gappiv         = 10;  /* vert inner gap between windows */
static const unsigned int gappoh         = 10;  /* horiz outer gap between windows and screen edge */
static const unsigned int gappov         = 30;  /* vert outer gap between windows and screen edge */
static const int smartgaps_fact          = 1;   /* gap factor when there is only one client; 0 = no gaps, 3 = 3x outer gaps */
static const int showbar                 = 1;   /* 0 means no bar */
static const int topbar                  = 1;   /* 0 means bottom bar */
/* Status is to be shown on: -1 (all monitors), 0 (a specific monitor by index), 'A' (active monitor) */
static const int statusmon               = 'A';
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int showsystray             = 1;   /* 0 means no systray */


/* Indicators: see patch/bar_indicators.h for options */
static int tagindicatortype              = INDICATOR_TOP_LEFT_SQUARE;
static int tiledindicatortype            = INDICATOR_NONE;
static int floatindicatortype            = INDICATOR_TOP_LEFT_SQUARE;
static const char *fonts[]               = { "monospace:size=15", "NotoColorEmoji:pixelsize=15:antialias=true:autohint=true" };
static const char dmenufont[]            = "monospace:size=15";

static char c000000[]                    = "#000000"; // placeholder value

static char normfgcolor[]                = "#ebdbb2";
static char normbgcolor[]                = "#282828";
static char normbordercolor[]            = "#928374";
static char normfloatcolor[]             = "#a89984";

static char selfgcolor[]                 = "#fbf1c7";
static char selbgcolor[]                 = "#cc241d";
static char selbordercolor[]             = "#cc241d";
static char selfloatcolor[]              = "#cc241d";

static char titlenormfgcolor[]           = "#ebdbb2";
static char titlenormbgcolor[]           = "#282828";
static char titlenormbordercolor[]       = "#928374";
static char titlenormfloatcolor[]        = "#a89984";

static char titleselfgcolor[]            = "#fbf1c7";
static char titleselbgcolor[]            = "#cc241d";
static char titleselbordercolor[]        = "#cc241d";
static char titleselfloatcolor[]         = "#cc241d";

static char tagsnormfgcolor[]            = "#ebdbb2";
static char tagsnormbgcolor[]            = "#282828";
static char tagsnormbordercolor[]        = "#928374";
static char tagsnormfloatcolor[]         = "#a89984";

static char tagsselfgcolor[]             = "#fbf1c7";
static char tagsselbgcolor[]             = "#cc241d";
static char tagsselbordercolor[]         = "#cc241d";
static char tagsselfloatcolor[]          = "#cc241d";

static char hidnormfgcolor[]             = "#005577";
static char hidselfgcolor[]              = "#227799";
static char hidnormbgcolor[]             = "#222222";
static char hidselbgcolor[]              = "#222222";

static char urgfgcolor[]                 = "#bbbbbb";
static char urgbgcolor[]                 = "#222222";
static char urgbordercolor[]             = "#ff0000";
static char urgfloatcolor[]              = "#db8fd9";




static char *colors[][ColCount] = {
	/*                       fg                bg                border                float */
	[SchemeNorm]         = { normfgcolor,      normbgcolor,      normbordercolor,      normfloatcolor },
	[SchemeSel]          = { selfgcolor,       selbgcolor,       selbordercolor,       selfloatcolor },
	[SchemeTitleNorm]    = { titlenormfgcolor, titlenormbgcolor, titlenormbordercolor, titlenormfloatcolor },
	[SchemeTitleSel]     = { titleselfgcolor,  titleselbgcolor,  titleselbordercolor,  titleselfloatcolor },
	[SchemeTagsNorm]     = { tagsnormfgcolor,  tagsnormbgcolor,  tagsnormbordercolor,  tagsnormfloatcolor },
	[SchemeTagsSel]      = { tagsselfgcolor,   tagsselbgcolor,   tagsselbordercolor,   tagsselfloatcolor },
	[SchemeHidNorm]      = { hidnormfgcolor,   hidnormbgcolor,   c000000,              c000000 },
	[SchemeHidSel]       = { hidselfgcolor,    hidselbgcolor,    c000000,              c000000 },
	[SchemeUrg]          = { urgfgcolor,       urgbgcolor,       urgbordercolor,       urgfloatcolor },
};




const char *spcmd1[] = {DEFAULT_TERMINAL, "-n", "spterm", "-g", "120x34", NULL };
const char *spcmd2[] = {DEFAULT_TERMINAL, "-n", "spcalc", "-f", "monospace:size=16", "-g", "50x20", "-e", "bc", "-lq", NULL };
static Sp scratchpads[] = {
   /* name          cmd  */
   {"spterm",      spcmd1},
   {"spcalc",      spcmd2},
};

/* Tags
 * In a traditional dwm the number of tags in use can be changed simply by changing the number
 * of strings in the tags array. This build does things a bit different which has some added
 * benefits. If you need to change the number of tags here then change the NUMTAGS macro in dwm.c.
 *
 * Examples:
 *
 *  1) static char *tagicons[][NUMTAGS*2] = {
 *         [DEFAULT_TAGS] = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I" },
 *     }
 *
 *  2) static char *tagicons[][1] = {
 *         [DEFAULT_TAGS] = { "•" },
 *     }
 *
 * The first example would result in the tags on the first monitor to be 1 through 9, while the
 * tags for the second monitor would be named A through I. A third monitor would start again at
 * 1 through 9 while the tags on a fourth monitor would also be named A through I. Note the tags
 * count of NUMTAGS*2 in the array initialiser which defines how many tag text / icon exists in
 * the array. This can be changed to *3 to add separate icons for a third monitor.
 *
 * For the second example each tag would be represented as a bullet point. Both cases work the
 * same from a technical standpoint - the icon index is derived from the tag index and the monitor
 * index. If the icon index is is greater than the number of tag icons then it will wrap around
 * until it an icon matches. Similarly if there are two tag icons then it would alternate between
 * them. This works seamlessly with alternative tags and alttagsdecoration patches.
 */
static char *tagicons[][NUMTAGS] =
{
	[DEFAULT_TAGS]        = { "1", "2", "3", "4", "5", "6", "7", "8", "9" },
	[ALTERNATIVE_TAGS]    = { "A", "B", "C", "D", "E", "F", "G", "H", "I" },
	[ALT_TAGS_DECORATION] = { "<1>", "<2>", "<3>", "<4>", "<5>", "<6>", "<7>", "<8>", "<9>" },
};


/* There are two options when it comes to per-client rules:
 *  - a typical struct table or
 *  - using the RULE macro
 *
 * A traditional struct table looks like this:
 *    // class      instance  title  wintype  tags mask  isfloating  monitor
 *    { "Gimp",     NULL,     NULL,  NULL,    1 << 4,    0,          -1 },
 *    { "Firefox",  NULL,     NULL,  NULL,    1 << 7,    0,          -1 },
 *
 * The RULE macro has the default values set for each field allowing you to only
 * specify the values that are relevant for your rule, e.g.
 *
 *    RULE(.class = "Gimp", .tags = 1 << 4)
 *    RULE(.class = "Firefox", .tags = 1 << 7)
 *
 * Refer to the Rule struct definition for the list of available fields depending on
 * the patches you enable.
 */
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 *	WM_WINDOW_ROLE(STRING) = role
	 *	_NET_WM_WINDOW_TYPE(ATOM) = wintype
	 */
	RULE(.wintype = WTYPE "DIALOG", .isfloating = 1)
	RULE(.wintype = WTYPE "UTILITY", .isfloating = 1)
	RULE(.wintype = WTYPE "TOOLBAR", .isfloating = 1)
	RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)
	RULE(.class = "ru-turikhay-tlauncher-bootstrap-Bootstrap", .isfloating = 1)
	RULE(.class = "Gimp", .tags = 1 << 4)
	RULE(.class = "Firefox", .tags = 1 << 7)
	RULE(.class = "St", .isterminal = 1)
	RULE(.class = "TelegramDesktop", .isfloating = 1)
	RULE(.class = "Minecraft* 1.19.2", .isfloating = 1)
	RULE(.instance = "spterm", .tags = SPTAG(0), .isfloating = 1, .isterminal = 1)
	RULE(.instance = "spcalc", .tags = SPTAG(1), .isfloating = 1)
};



/* Bar rules allow you to configure what is shown where on the bar, as well as
 * introducing your own bar modules.
 *
 *    monitor:
 *      -1  show on all monitors
 *       0  show on monitor 0
 *      'A' show on active monitor (i.e. focused / selected) (or just -1 for active?)
 *    bar - bar index, 0 is default, 1 is extrabar
 *    alignment - how the module is aligned compared to other modules
 *    widthfunc, drawfunc, clickfunc - providing bar module width, draw and click functions
 *    name - does nothing, intended for visual clue and for logging / debugging
 */
static const BarRule barrules[] = {
	/* monitor   bar    alignment         widthfunc                 drawfunc                clickfunc                hoverfunc                name */
	{ -1,        0,     BAR_ALIGN_LEFT,   width_tags,               draw_tags,              click_tags,              hover_tags,              "tags" },
	{  0,        0,     BAR_ALIGN_RIGHT,  width_systray,            draw_systray,           click_systray,           NULL,                    "systray" },
	{ -1,        0,     BAR_ALIGN_LEFT,   width_ltsymbol,           draw_ltsymbol,          click_ltsymbol,          NULL,                    "layout" },
	{ statusmon, 0,     BAR_ALIGN_RIGHT,  width_status2d,           draw_status2d,          click_statuscmd,         NULL,                    "status2d" },
	{ -1,        0,     BAR_ALIGN_NONE,   width_wintitle,           draw_wintitle,          click_wintitle,          NULL,                    "wintitle" },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */



static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "|||",      col },

	{ "(@)",      spiral },
	{ "[\\]",     dwindle },

	{ "TTT",      bstack },
	{ "===",      bstackhoriz },

	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },

	{ "[D]",      deck },
	{ "[M]",      monocle },

	{ "><>",      NULL },    /* no layout function means floating behavior */
};


/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },



/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *dmenucmd[] = {
	"dmenu_run",
	"-fn", dmenufont,
	"-nb", normbgcolor,
	"-nf", normfgcolor,
	"-sb", selbgcolor,
	"-sf", selfgcolor,
	"-l", "10",
	NULL
};
static const char *termcmd[]  = { DEFAULT_TERMINAL, NULL };

/* This defines the name of the executable that handles the bar (used for signalling purposes) */
#define STATUSBAR "dwmblocks"

#include <X11/XF86keysym.h>

static const Key keys[] = {
	/* modifier                     key            function                argument */
	// apps
	{ MODKEY,                       XK_q,          killclient,             {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,          quit,                   {1} },
	{ MODKEY|ShiftMask,             XK_q,          quit,                   {0} },

	{ MODKEY,                       XK_d,          spawn,                  {.v = dmenucmd } },
	{ MODKEY,                       XK_Return,     spawn,                  {.v = termcmd } },
	{ MODKEY,                       XK_b,          togglebar,              {0} },
	{ MODKEY,                       XK_w,          spawn,                  {.v = (const char*[]){ BROWSER, NULL } } },
	{ MODKEY|ShiftMask,             XK_w,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "sudo", "nmtui", NULL } } },
	{ MODKEY,                       XK_r,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "lfub", NULL } } },
	{ MODKEY|ShiftMask,             XK_r,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "htop", NULL } } },
	{ MODKEY|ShiftMask,             XK_n,          spawn,                  SHCMD(DEFAULT_TERMINAL " -e newsboat ; kill -40 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_e,          spawn,                  SHCMD(DEFAULT_TERMINAL " -e neomutt ; kill -46 $(pidof dwmblocks); rmdir ~/.abook") },

	// larbs.xyz features
	{ MODKEY,                       XK_F1,         spawn,                  SHCMD("groff -mom /usr/local/share/dwm/larbs.mom -Tpdf | zathura -") },
	{ MODKEY,                       XK_F2,         spawn,                  {.v = (const char*[]){ "tutorialvids", NULL } } },
	{ MODKEY,                       XK_F3,         spawn,                  {.v = (const char*[]){ "displayselect", NULL } } },
	{ MODKEY,                       XK_F4,         spawn,                  SHCMD(DEFAULT_TERMINAL " -e pulsemixer; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_F5,         xrdb,                   {.v = NULL } },
	{ MODKEY,                       XK_F6,         spawn,                  {.v = (const char*[]){ "torwrap", NULL } } },
	{ MODKEY,                       XK_F7,         spawn,                  {.v = (const char*[]){ "td-toggle", NULL } } },
	{ MODKEY,                       XK_F8,         spawn,                  {.v = (const char*[]){ "mw", "-Y", NULL } } },
	{ MODKEY,                       XK_F9,         spawn,                  {.v = (const char*[]){ "dmenumount", NULL } } },
	{ MODKEY,                       XK_F10,        spawn,                  {.v = (const char*[]){ "dmenuumount", NULL } } },
	{ MODKEY,                       XK_F11,        spawn,                  SHCMD("mpv --untimed --no-cache --no-osc --no-input-default-bindings --profile=low-latency --input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | tail -n 1)") },
	{ MODKEY,                       XK_F12,        spawn,                  SHCMD("remaps") },
	{ MODKEY,                       XK_Insert,     spawn,                  SHCMD("xdotool type $(grep -v '^#' ~/.local/share/personal/bookmarks | dmenu -i -l 50 | cut -d' ' -f1)") },
	{ MODKEY,                       XK_BackSpace,  spawn,                  {.v = (const char*[]){ "sysact", NULL } } },
	{ MODKEY,                       XK_grave,      spawn,                  {.v = (const char*[]){ "dmenuunicode", NULL } } },

	// screenshot & record
	{ 0,                            XK_Print,      spawn,                  SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ ShiftMask,                    XK_Print,      spawn,                  {.v = (const char*[]){ "maimpick", NULL } } },
	{ MODKEY,                       XK_Print,      spawn,                  {.v = (const char*[]){ "dmenurecord", NULL } } },
	{ MODKEY|ShiftMask,             XK_Print,      spawn,                  {.v = (const char*[]){ "dmenurecord", "kill", NULL } } },
	{ MODKEY,                       XK_Delete,     spawn,                  {.v = (const char*[]){ "dmenurecord", "kill", NULL } } },
	{ MODKEY,                       XK_Scroll_Lock, spawn,                  SHCMD("killall screenkey || screenkey &") },

	// sound and media
	{ MODKEY,                       XK_m,          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "ncmpcpp", NULL } } },
	{ MODKEY|ShiftMask,             XK_m,          spawn,                  SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_comma,      spawn,                  {.v = (const char*[]){ "mpc", "prev", NULL } } },
	{ MODKEY,                       XK_period,     spawn,                  {.v = (const char*[]){ "mpc", "next", NULL } } },
	{ MODKEY,                     XK_bracketleft,  spawn,                  {.v = (const char*[]){ "mpc", "seek", "-10", NULL } } },
	{ MODKEY|ShiftMask,           XK_bracketleft,  spawn,                  {.v = (const char*[]){ "mpc", "seek", "-60", NULL } } },
	{ MODKEY,                     XK_bracketright, spawn,                  {.v = (const char*[]){ "mpc", "seek", "+10", NULL } } },
	{ MODKEY|ShiftMask,           XK_bracketright, spawn,                  {.v = (const char*[]){ "mpc", "seek", "+60", NULL } } },
	{ MODKEY,                       XK_p,          spawn,                  {.v = (const char*[]){ "mpc", "toggle", NULL } } },
	{ MODKEY|ShiftMask,             XK_p,          spawn,                  SHCMD("mpc pause; pauseallmpv") },
	{ MODKEY,                       XK_minus,      spawn,                  SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_minus,      spawn,                  SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_equal,      spawn,                  SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_equal,      spawn,                  SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)") },

	// stacks
	{ MODKEY,                       XK_v,          focusstack,             {.i =  0 } },
	{ MODKEY,                       XK_j,          focusstack,             {.i = +1 } },
	{ MODKEY,                       XK_k,          focusstack,             {.i = -1 } },
	{ MODKEY,                       XK_h,          setmfact,               {.f = -0.05} },
	{ MODKEY,                       XK_l,          setmfact,               {.f = +0.05} },

	{ MODKEY|ShiftMask,             XK_h,          setcfact,               {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_l,          setcfact,               {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_BackSpace,  setcfact,               {0} },
	{ MODKEY|ShiftMask,             XK_j,          movestack,              {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,          movestack,              {.i = -1 } },
	{ MODKEY,                       XK_space,      zoom,                   {0} },

	// gaps
	{ MODKEY,                       XK_z,          incrgaps,               {.i = +3 } },
	{ MODKEY,                       XK_x,          incrgaps,               {.i = -3 } },
	{ MODKEY,                       XK_a,          togglegaps,             {0} },
	{ MODKEY|ShiftMask,             XK_a,          defaultgaps,            {0} },

	// layouts
	{ MODKEY,                       XK_t,          setlayout,              {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_t,          setlayout,              {.v = &layouts[1]} },
	{ MODKEY,                       XK_y,          setlayout,              {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_y,          setlayout,              {.v = &layouts[3]} },
	{ MODKEY,                       XK_u,          setlayout,              {.v = &layouts[4]} },
	{ MODKEY|ShiftMask,             XK_u,          setlayout,              {.v = &layouts[5]} },
	{ MODKEY,                       XK_i,          setlayout,              {.v = &layouts[6]} },
	{ MODKEY|ShiftMask,             XK_i,          setlayout,              {.v = &layouts[7]} },
	{ MODKEY,                       XK_o,          incnmaster,             {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_o,          incnmaster,             {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_f,          setlayout,              {.v = &layouts[10]} },
	/* { MODKEY|ShiftMask,             XK_f,          setlayout,              {0} }, */
	{ MODKEY|ShiftMask,             XK_space,      togglefloating,         {0} },

	// scratch & sticky
	{ MODKEY|ShiftMask,             XK_Return,     togglescratch,          {.ui = 0 } },
	{ MODKEY,                       XK_apostrophe, togglescratch,          {.ui = 1 } },
	{ MODKEY|ControlMask,           XK_grave,      setscratch,             {.ui = 0 } },
	{ MODKEY|ShiftMask,             XK_grave,      removescratch,          {.ui = 0 } },
	{ MODKEY,                       XK_f,          togglefullscreen,       {0} },
	{ MODKEY,                       XK_s,          togglesticky,           {0} },

	// monitors
	{ MODKEY,                       XK_Left,       focusmon,               {.i = -1 } },
	{ MODKEY,                       XK_Right,      focusmon,               {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Left,       tagmon,                 {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Right,      tagmon,                 {.i = +1 } },

	// tags
	{ MODKEY,                       XK_Tab,        view,                   {0} },
	{ MODKEY,                       XK_g,          shiftviewclients,       { .i = -1 } },
	{ MODKEY,                       XK_semicolon,  shiftviewclients,       { .i = +1 } },
	{ MODKEY,                       XK_0,          view,                   {.ui = ~SPTAGMASK } },
	{ MODKEY|ShiftMask,             XK_0,          tag,                    {.ui = ~SPTAGMASK } },
	TAGKEYS(                        XK_1,                                  0)
	TAGKEYS(                        XK_2,                                  1)
	TAGKEYS(                        XK_3,                                  2)
	TAGKEYS(                        XK_4,                                  3)
	TAGKEYS(                        XK_5,                                  4)
	TAGKEYS(                        XK_6,                                  5)
	TAGKEYS(                        XK_7,                                  6)
	TAGKEYS(                        XK_8,                                  7)
	TAGKEYS(                        XK_9,                                  8)

	// function keys
	{ 0, XF86XK_AudioMute,                         spawn,                  SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioRaiseVolume,                  spawn,                  SHCMD("pamixer --allow-boost -i 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioLowerVolume,                  spawn,                  SHCMD("pamixer --allow-boost -d 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioPrev,                         spawn,                  {.v = (const char*[]){ "mpc", "prev", NULL } } },
	{ 0, XF86XK_AudioNext,                         spawn,                  {.v = (const char*[]){ "mpc",  "next", NULL } } },
	{ 0, XF86XK_AudioPause,                        spawn,                  {.v = (const char*[]){ "mpc", "pause", NULL } } },
	{ 0, XF86XK_AudioPlay,                         spawn,                  {.v = (const char*[]){ "mpc", "toggle", NULL } } },
	{ 0, XF86XK_AudioStop,                         spawn,                  {.v = (const char*[]){ "mpc", "stop", NULL } } },
	{ 0, XF86XK_AudioRewind,                       spawn,                  {.v = (const char*[]){ "mpc", "seek", "-10", NULL } } },
	{ 0, XF86XK_AudioForward,                      spawn,                  {.v = (const char*[]){ "mpc", "seek", "+10", NULL } } },
	{ 0, XF86XK_AudioMedia,                        spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "ncmpcpp", NULL } } },
	{ 0, XF86XK_AudioMicMute,                      spawn,                  SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ 0, XF86XK_Calculator,                        spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "bc", "-l", NULL } } },
	{ 0, XF86XK_Sleep,                             spawn,                  {.v = (const char*[]){ "sudo", "-A", "zzz", NULL } } },
	{ 0, XF86XK_WWW,                               spawn,                  {.v = (const char*[]){ BROWSER, NULL } } },
	{ 0, XF86XK_DOS,                               spawn,                  {.v = termcmd } },
	{ 0, XF86XK_ScreenSaver,                       spawn,                  SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_TaskPane,                          spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e", "htop", NULL } } },
	{ 0, XF86XK_Mail,                              spawn,                  SHCMD(DEFAULT_TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks") },
	{ 0, XF86XK_MyComputer,                        spawn,                  {.v = (const char*[]){ DEFAULT_TERMINAL, "-e",  "lfub",  "/", NULL } } },
	{ 0, XF86XK_Launch1,                           spawn,                  {.v = (const char*[]){ "xset", "dpms", "force", "off", NULL } } },
	{ 0, XF86XK_TouchpadToggle,                    spawn,                  SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOff,                       spawn,                  {.v = (const char*[]){ "synclient", "TouchpadOff=1", NULL } } },
	{ 0, XF86XK_TouchpadOn,                        spawn,                  {.v = (const char*[]){ "synclient", "TouchpadOff=0", NULL } } },
	{ 0, XF86XK_MonBrightnessUp,                   spawn,                  {.v = (const char*[]){ "xbacklight", "-inc", "15", NULL } } },
	{ 0, XF86XK_MonBrightnessDown,                 spawn,                  {.v = (const char*[]){ "xbacklight", "-dec", "15", NULL } } },
	/* { 0, XF86XK_Battery,		spawn,		SHCMD("") }, */
	/* { 0, XF86XK_PowerOff,		spawn,		{.v = (const char*[]){ "sysact", NULL } } }, */
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask           button          function        argument */
	{ ClkLtSymbol,          0,                   Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,                   Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,                   Button2,        zoom,           {0} },
	{ ClkStatusText,        0,                   Button1,        sigstatusbar,   {.i = 1 } },
	{ ClkStatusText,        0,                   Button2,        sigstatusbar,   {.i = 2 } },
	{ ClkStatusText,        0,                   Button3,        sigstatusbar,   {.i = 3 } },
	{ ClkClientWin,         MODKEY,              Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,              Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,              Button3,        resizemouse,    {0} },
	{ ClkClientWin,         MODKEY|ShiftMask,    Button3,        dragcfact,      {0} },
	{ ClkClientWin,         MODKEY|ShiftMask,    Button1,        dragmfact,      {0} },
	{ ClkTagBar,            0,                   Button1,        view,           {0} },
	{ ClkTagBar,            0,                   Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,              Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,              Button3,        toggletag,      {0} },
};

