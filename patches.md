# list the patches

## wishlists

- [x] swallow
- [x] xresources
- [x] systray
- [x] restart
- [x] fullgaps
- [x] zoomswap
- [x] cfacts
- [x] layout
  - [x] bottomstack
  - [x] centeredmaster
- [x] window
  - [x] center
  - [x] attachbelow
  - [x] togglefullscr
  - [x] sizehint
  - [x] windowrolerules

- [x] status2d
- [x] statuscmd

## thoughts

I don't want to install scratchpad and sticky right now.

- scratchpad: I want to using my computer without it for a period of time
to see if I really need it. A feeling I have now that I won't really.
- sticky: This I shouldn't need it cuz if I want something to appear on all tags
I can just assign it to all tags.
